package homework1;

import java.util.Random;
import java.util.Scanner;

public class NumbersQuiz {

    public static final String EXIT_COMMAND = "exit";
    public static final int YEAR_INDEX = 0;
    public static final int QUESTION_INDEX = 1;
    private static Random random;
    private static String[][] historicalEvents = {
            {"1969", "Apollo 11 lands on the Moon... What's the year?"},
            {"1981", "Prince Charles and Lady Diana Spencer get married… What’s the year?"},
            {"1985", "The Live Aid benefit concert attracts 72,000 people in Wembley Stadium… What’s the year?"},
            {"1970", "It’s widely considered to be the year the Beatles split…"},
            {"2007", "The first iPhone has just been released... What's the year?"},
            {"1975", "Jaws hits cinema screens... What's the year?"},
            {"1998", "American search engine company Google is officially launched... What's the year?"},
            {"1982", "The first international Rubik’s Cube Championships take place… What’s the year?"},
            {"1966", "England beats West Germany to win the FIFA World Cup… What’s the year?"},
            {"2004", "Facebook is founded... What's the year?"}
    };

    private static String[] roundsResults = new String[0];
    private static String name;
    private static int askedQuestionIndex;
    private static Scanner scan;


    public static void main(String[] args) {
        scan = new Scanner(System.in);
        random = new Random();
        intruductTheGame();
        playTheGame();
        beforeExit();
    }

    private static void playTheGame() {
        askNextQuestion();
        while (true) {
            if (scan.hasNextInt()) {
                int correctYear = Integer.parseInt(historicalEvents[askedQuestionIndex][YEAR_INDEX]);
                int guessYear = scan.nextInt();
                boolean isCorrect = guessYear == correctYear;


                String result;
                if (isCorrect) {
                    result = "Correct";
                    System.out.printf("Congratulations %s!\n", name);
                    askNextQuestion();
                } else {
                    result = guessYear < correctYear ? "too small" : "too big";
                    System.out.printf("Your number is %s. Please, try again.\n", result);
                }
                saveResult(guessYear + " - " + result);

            } else {
                String input = scan.next();
                if (shouldExit(input)) {
                    break;
                } else {
                    System.out.println("Wrong number format. Please try again.");
                }
            }
        }
    }

    private static void saveResult(String result) {
        String[] newArr = new String[roundsResults.length + 1];
        System.arraycopy(roundsResults, 0, newArr, 0, roundsResults.length);
        roundsResults = newArr;
        roundsResults[roundsResults.length - 1] = result;
    }

    private static void intruductTheGame() {
        System.out.println(
                "Dates quiz. Try to guess the years of historical events. \n" +
                        "To finish the game type: exit\n" +
                        "Let the game begin!");

        System.out.println("What is your name?");

        name = scan.nextLine();
    }

    private static void askNextQuestion() {
        askedQuestionIndex = random.nextInt(historicalEvents.length);
        System.out.println("The question is: \n" + historicalEvents[askedQuestionIndex][QUESTION_INDEX] + "(debug: " + historicalEvents[askedQuestionIndex][YEAR_INDEX] + ")");
    }

    private static void beforeExit() {
        System.out.println("Your results: ");
        for (String roundsResult : roundsResults) {
            System.out.println(roundsResult);
        }
        System.out.printf("See you, %s", name);
    }

    private static boolean shouldExit(String input) {
        return EXIT_COMMAND.equals(input.toLowerCase());
    }
}
